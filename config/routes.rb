Rails.application.routes.draw do

  root 'payments/index'
  resources :students
  resources :teachers
  resources :exams
  resources :reports
  resources :pockets
  resources :payments
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
