class StudentsController < ApplicationController
  def new #untuk menampilkan form data baru
    @student = Student.new
  end    
  def create #untuk memperoses data baru yang dimasukkan di form new
    student = Student.new (resources_params)
    student.save 
    redirect_to students_path
    flash[:notice] = 'Student has been created'
  end   
  def edit #menampilkan data yang sudah disimpan
    @student = Student.find(params[:id])
  end  
  def update #melakukan proses ketika user mengedit data
  @student = Student.find(params[:id])
  @student.update(resources_params)
  flash[:notice] = 'Student has been update'
  redirect_to student_path(@student)
  end    
  def destroy #untuk menghapus data
    @student = Student.find(params[:id])
    @student.destroy
    flash[:notice] = 'Student has been destroy'
    redirect_to students_path(@student)
  end  
  def index #menampilkan seluruh data yang ada di database
    @students = Student.all
  end    
  def show #menampilkan  sebuah data sectemplateara detail
    id = params[:id]
    @student = Student.find(id)
    # render plain: id
    # render plain: @book.title 
   end
  private    
  def resources_params
    params.required(:student).permit(:name, :username, :age, :kelas, :address, :city, :NIK)
  end
end
