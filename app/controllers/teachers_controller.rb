class TeachersController < ApplicationController
  def new #untuk menampilkan form data baru
    @teacher = Teacher.new
  end    
  def create #untuk memperoses data baru yang dimasukkan di form new
    teacher = Teacher.new (resources_params)
    teacher.save 
    redirect_to teachers_path
    flash[:notice] = 'teacher has been created'
  end   
  def edit #menampilkan data yang sudah disimpan
    @teacher = Teacher.find(params[:id])
  end  
  def update #melakukan proses ketika user mengedit data
  @teacher = Teacher.find(params[:id])
  @teacher.update(resources_params)
  flash[:notice] = 'teacher has been update'
  redirect_to teacher_path(@teacher)
  end    
  def destroy #untuk menghapus data
    @teacher = Teacher.find(params[:id])
    @teacher.destroy
    flash[:notice] = 'teacher has been destroy'
    redirect_to teachers_path(@teacher)
  end  
  def index #menampilkan seluruh data yang ada di database
    @teachers = Teacher.all
  end    
  def show #menampilkan  sebuah data sectemplateara detail
    id = params[:id]
    @teacher = Teacher.find(id)
    # render plain: id
    # render plain: @book.title 
   end
  private    
  def resources_params
    params.required(:teacher).permit(:NIK, :name, :age, :kelas, :mapel)
  end
end
