class ReportsController < ApplicationController
    def new #untuk menampilkan form data baru
      @report = Report.new
    end    
    def create #untuk memperoses data baru yang dimasukkan di form new
      report = Report.new (resources_params)
      report.save 
      redirect_to reports_path
      flash[:notice] = 'report has been created'
    end   
    def edit #menampilkan data yang sudah disimpan
      @report = Report.find(params[:id])
    end  
    def update #melakukan proses ketika user mengedit data
    @report = Report.find(params[:id])
    @report.update(resources_params)
    flash[:notice] = 'report has been update'
    redirect_to report_path(@report)
    end    
    def destroy #untuk menghapus data
      @report = Report.find(params[:id])
      @report.destroy
      flash[:notice] = 'report has been destroy'
      redirect_to reports_path(@report)
    end  
    def index #menampilkan seluruh data yang ada di database
      @reports = Report.all
    end    
    def show #menampilkan  sebuah data sectemplateara detail
      id = params[:id]
      @report = Report.find(id)
      # render plain: id
      # render plain: @book.title 
     end
    private    
    def resources_params
      params.required(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :date)
    end
  end
