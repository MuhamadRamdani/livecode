class PocketsController < ApplicationController
  def new #untuk menampilkan form data baru
    @pocket = Pocket.new
  end    
  def create #untuk memperoses data baru yang dimasukkan di form new
    pocket = Pocket.new (resources_params)
    pocket.save 
    redirect_to pockets_path
    flash[:notice] = 'pocket has been created'
  end   
  def edit #menampilkan data yang sudah disimpan
    @pocket = Pocket.find(params[:id])
  end  
  def update #melakukan proses ketika user mengedit data
  @pocket = Pocket.find(params[:id])
  @pocket.update(resources_params)
  flash[:notice] = 'pocket has been update'
  redirect_to pocket_path(@pocket)
  end    
  def destroy #untuk menghapus data
    @pocket = Pocket.find(params[:id])
    @pocket.destroy
    flash[:notice] = 'pocket has been destroy'
    redirect_to pockets_path(@pocket)
  end  
  def index #menampilkan seluruh data yang ada di database
    @pockets = Pocket.all
  end    
  def show #menampilkan  sebuah data sectemplateara detail
    id = params[:id]
    @pocket = Pocket.find(id)
    # render plain: id
    # render plain: @book.title 
   end
  private    
  def resources_params
    params.required(:pocket).permit(:balance, :student_id, :teacher_id )
  end
end
