class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.string :NIK
      t.string :name
      t.integer :age
      t.string :kelas
      t.string :mapel

      t.timestamps
    end
  end
end
